#include "ToreCannonic.h"

#define NTore 100
#define PTore 100

int nTore = 54, pTore = 50;
double hauteurTore = 1;

void ToreCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(NTore*PTore, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(NTore*PTore, sizeof (G3Xvector));
  double theta = 2 * PI / PTore;
  double phi = 3 * PI / NTore;
  double cosiphi, siniphi;
  int i, j;
  /**
   * initialisation des matrices de transformations
   */
  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 1;
  body.ambi = 0, 2;
  body.diff = 0.3;
  body.spec = 0.4;
  body.shine = 0.5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {0.0, 1.0, 1.0, 0.0};

  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];

  double radius = 0.5;
  /**
   * on etablit les caracteristiques physiques de l'objet
   */
  for (i = 0; i < NTore; i++) {
    cosiphi = cos(i * phi);
    siniphi = sin(i * phi);
    for (j = 0; j < PTore; j++) {

      body.vrtx[i * PTore + j][0] = cos(j * theta)*(1 + radius * cosiphi);
      body.norm[i * PTore + j][0] = body.vrtx[i * PTore + j][0] - cosiphi;
      body.vrtx[i * PTore + j][1] = sin(j * theta) *(1 + radius * cosiphi);
      body.norm[i * PTore + j][1] = body.vrtx[i * PTore + j][1] - (siniphi);
      body.vrtx[i * PTore + j][2] = radius*siniphi;
      //body.norm[i * PTore + j][2]=body.vrtx[i * PTore + j][2];
    }
  }
  body.scalex = 10;
  body.scaley = 10;
  memcpy(body.norm, body.vrtx, NTore * PTore * sizeof (G3Xvector));

  //g3x_CreateScrollv_i("nt", &nTore, 3, NTore, 1.0, " ");
  //g3x_CreateScrollv_i("pt", &pTore, 3, PTore, 1.0, " ");
  //g3x_CreateScrollh_d("rt", &hauteurTore, 0.1, 10, 1.0, " ");

}

void ToreCannonic::Draw() {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  int k;
  int stepn = NTore / nTore;
  int stepp = PTore / pTore;
  int i, j;
  for (i = 0; i < NTore - stepn; i += stepn) {
    for (j = 0; j < PTore - stepp; j += stepp) {
      k = i * PTore + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = i * PTore + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PTore + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PTore + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PTore + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = i*PTore;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PTore;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PTore + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }

  glEnd();
}

void ToreCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void ToreCannonic::Draw(HMat vrtx, HMat norm) {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  int k;
  int stepn = NTore / nTore;
  int stepp = PTore / pTore;
  G3Xpoint normal;
  G3Xvector vertex;
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  int i, j;
  for (i = 0; i < NTore - stepn; i += stepn) {
    for (j = 0; j < PTore - stepp; j += stepp) {
      k = i * PTore + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = i * PTore + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * PTore + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * PTore + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PTore + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = i*PTore;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PTore;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PTore + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }
  glEnd();
}

void ToreCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
