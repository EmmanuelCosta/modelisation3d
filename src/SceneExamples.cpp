#include <array>
#include <iostream>

#include "Scene.cpp"

class Board : public Object {
  public:
    Board(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CubeCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class RoundBoard : public Object {
  public:
    RoundBoard(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CylinderCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class Foot : public Object {
  public:
    Foot(array<double, 3> pos_, double r_, double h_) : Object(pos_) {
      pos = pos_;
      r = r_;
      h = h_;
    }

    void init() {
      canonical_o = new CylinderCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, r, r, h / 2, true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  protected:
    double r;
    double h;
};

class WindTrunc : public Object {
  public:
    WindTrunc(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CylinderCannonic();
      canonical_o->Init();
      canonical_o->setColor(red);
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, -size[0] - 0.009, size[1], size[2] + 0.1, true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class WindBool : public Object {
  public:
    WindBool(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new SphereCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, -size[0] - 0.1, size[1] + 0.01, -size[2] - 0.1, false);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class BedTrunk : public Object {
  public:
    BedTrunk(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new SphereCannonic();
      canonical_o->Init();
      canonical_o->setColor(cyan);
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class WindScrew : public Object {
  public:
    WindScrew(array<double, 3> pos_, array<double, 3> size_, double angle_) : Object(pos_) {
      pos = pos_;
      size = size_;
      angle = angle_;
    }

    void init() {
      canonical_o = new ConeCannonic();
      canonical_o->Init();
      canonical_o->setColor(blue);
      HMat temp, tmp;
      Identity(canonical_o->body.Md);
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      RotationX(temp, angle, true);
      Translation(tmp, vec, true);
      Scaling(transform, size[0], size[1], size[2], true);
      ProdMatMat2(temp, tmp);
      ProdMatMat2(transform, temp);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
    double angle;
    HMat initObject;
};

class Wind : public MacroObject {
  public:
    Wind(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      pos = pos_;
      WindTrunc* windTrunc = new WindTrunc({pos[0], pos[1], pos[2]},
          {
          size[0], size[1], size[2]
          });
      windTrunc->init();

      WindBool* windBool = new WindBool({pos[0] + 0.1, pos[1], pos[2]+7*size[0]},
          {
          0.1, size[0], size[0]
          });
      windBool->init();

      WindScrew* windScrew1 = new WindScrew({pos[0] + 0.1, pos[1], pos[2]+7*size[0]},
          {
          size[0], size[1] - 0.1, size[2]
          }, 3.5);

      WindScrew* windScrew2 = new WindScrew({pos[0] + 0.1, pos[1], pos[2]+7*size[0]},
          {
          size[0], size[1] - 0.1, size[2]
          }, 90);

      WindScrew* windScrew3 = new WindScrew({pos[0] + 0.1, pos[1],pos[2]+7*size[0]},
          {
          size[0], size[1] - 0.1, size[2]
          }, 180);

      windScrew1->init();
      windScrew2->init();
      windScrew3->init();
      objects.push_back(windTrunc);
      objects.push_back(windBool);
      objects.push_back(windScrew1);
      objects.push_back(windScrew2);
      objects.push_back(windScrew3);
    }

};

class Table : public MacroObject {
  public:
    Table(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      Foot* foot1;
      Foot* foot2;
      Foot* foot3;
      Foot* foot4;
      Board* board;

      pos = pos_;

      double feet_height_pc = 95.0;
      double min_ = min(size[0], size[1]);

      double feet_height = feet_height_pc / 100.0 * size[2];
      double feet_radius = (100 - feet_height_pc) / 100.0 * min_;
      double board_height = size[2] - feet_height;

      board = new Board({pos[0], pos[1], pos[2] + feet_height + board_height / 2.0},
          {
          size[0], size[1], board_height
          });
      board->init();

      foot1 = new Foot({pos[0] + size[0] - (10.0 / 100 * min_),
          pos[1] + size[1] - (10.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot1->init();
      foot2 = new Foot({pos[0] + size[0] - (10.0 / 100 * min_),
          pos[1] - size[1] + (10.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot2->init();
      foot3 = new Foot({pos[0] - size[0] + (10.0 / 100 * min_),
          pos[1] + size[1] - (10.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot3->init();
      foot4 = new Foot({(pos[0] - size[0] + (10.0 / 100 * min_)),
          (pos[1] - size[1] + (10.0 / 100 * min_)),
          size[2] / 2},
          feet_radius, feet_height);
      foot4->init();

      objects.push_back(board);
      objects.push_back(foot1);
      objects.push_back(foot2);
      objects.push_back(foot3);
      objects.push_back(foot4);
    }
};

class Stool : public MacroObject {
  public:
    Stool(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      pos = pos_;

      double feet_height_pc = 95.0;
      double min_ = min(size[0], size[1]);

      double feet_height = feet_height_pc / 100.0 * size[2];
      double feet_radius = (100 - feet_height_pc) / 100.0 * min_;
      double board_height = size[2] - feet_height;

      board = new RoundBoard({pos[0], pos[1], pos[2] + feet_height + board_height / 2.0},
          {
          size[0], size[1], board_height
          });
      board->init();

      foot1 = new Foot({pos[0] + size[0] - (40.0 / 100 * min_),
          pos[1] + size[1] - (40.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot1->init();
      foot2 = new Foot({pos[0] + size[0] - (40.0 / 100 * min_),
          pos[1] - size[1] + (40.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot2->init();
      foot3 = new Foot({pos[0] - size[0] + (40.0 / 100 * min_),
          pos[1] + size[1] - (40.0 / 100 * min_),
          size[2] / 2},
          feet_radius, feet_height);
      foot3->init();
      foot4 = new Foot({(pos[0] - size[0] + (40.0 / 100 * min_)),
          (pos[1] - size[1] + (40.0 / 100 * min_)),
          size[2] / 2},
          feet_radius, feet_height);
      foot4->init();

      objects.push_back(board);
      objects.push_back(foot1);
      objects.push_back(foot2);
      objects.push_back(foot3);
      objects.push_back(foot4);
    }

  private:
    Foot* foot1;
    Foot* foot2;
    Foot* foot3;
    Foot* foot4;
    RoundBoard* board;
};

class BedBoard : public Object {
  public:
    BedBoard(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CubeCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * represente un lit
 * @param size_
 */
class Bed : public MacroObject {
  public:
    Bed(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      pos = pos_;
      G3Xcolor testcolor = {0.4, 0.7, 0.9};

      Board* bedTrunk = new Board({pos[0], pos[1], pos[2]},
          {size[0] + 0.2, size[1] - 0.1, size[2]});

      BedBoard* bedTrunk1 = new BedBoard({pos[0]+size[0] + 0.2, pos[1], pos[2]},
          {0.01, 0.9, 0.6});
      BedBoard* bedTrunk2 = new BedBoard({pos[0]-(size[0] + 0.2) , pos[1], pos[2]},
          {0.01, 0.9, 0.6});
      bedTrunk->init();
      bedTrunk1->init();
      bedTrunk2->init();

      bedTrunk->setColor(testcolor);
      objects.push_back(bedTrunk);
      objects.push_back(bedTrunk1);
      objects.push_back(bedTrunk2);
    }
};

/**
 * c'est la partie inferieure de la lampe(le pied sur lequel il repose)
 * @param pos_
 * @param size_
 */
class LampBottom : public Object {
  public:
    LampBottom(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new ToreCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
      RotationZ_custom(transform, -20, true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * c'est la partie de la lampe qui se pose audessus du pied
 * @param pos_
 * @param size_
 */
class LampBottomTrunk : public Object {
  public:
    LampBottomTrunk(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CylinderCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
      RotationZ_custom(transform, -20, true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * c'est la partie principale de la lampe,
 * @param pos_
 * @param size_
 */
class LampMiddleTrunk : public Object {
  public:
    LampMiddleTrunk(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new SphereCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
      RotationZ_custom(transform, -20, true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * C'est le chapeau du clown
 */
class ClownCap : public Object {
  public:
    ClownCap(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new ConeCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * c'est la lampe 
 */
class Lamp : public MacroObject {
  public:
    Lamp(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      pos = pos_;
      G3Xcolor testcolor = {0.4, 0.7, 0.9};

      LampBottom* lampBottom = new LampBottom({pos[0], pos[1], pos[2] - 0.1},
          {
          size[0], size[1], size[2]
          });
      lampBottom->init();
      LampBottomTrunk* lampBottomTrunk = new LampBottomTrunk({pos[0], pos[1], pos[2] + size[2] / 3},
          {
          size[0], size[1], size[2]
          });
      lampBottomTrunk->init();

      LampMiddleTrunk* lampMiddleTrunk = new LampMiddleTrunk({pos[0], pos[1], pos[2] + size[2] / 3 + size[2] + 0.1},
          {
          size[0] + 0.0999, size[1] + 0.0999, size[2] + 0.0999
          });
      lampMiddleTrunk->init();

      LampBottom* lampCap = new LampBottom({pos[0], pos[1], pos[2] + size[2] / 3 + size[2] + 0.1 + size[2] + 0.1},
          {
          size[0], size[1], size[2]
          });
      lampCap->init();

      objects.push_back(lampBottom);
      objects.push_back(lampMiddleTrunk);
      objects.push_back(lampBottomTrunk);
      objects.push_back(lampCap);
    }
};

/**
 * represente le visage du clown
 * @param pos_
 * @param size_
 */
class ClownFace : public Object {
  public:
    ClownFace(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new SphereCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * c'est le nez du clown
 * @param pos_
 * @param size_
 */
class ClownNeck : public Object {
  public:
    ClownNeck(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new CoilCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

class ClownTrunk : public Object {
  public:
    ClownTrunk(array<double, 3> pos_, array<double, 3> size_) : Object(pos_) {
      pos = pos_;
      size = size_;
    }

    void init() {
      canonical_o = new ToreCannonic();
      canonical_o->Init();
      vec[0] = pos[0];
      vec[1] = pos[1];
      vec[2] = pos[2];
      Translation(transform, vec, true);
      Scaling(canonical_o->body.Md, size[0], size[1], size[2], true);
      ProdMatMat2(canonical_o->body.Md, transform);
    }

    void draw() {
      canonical_o->Draw(canonical_o->body.Md, canonical_o->body.Md);
    }

  private:
    array<double, 3> size;
};

/**
 * cette objet sera le diablebox
 * @param size_
 */
class DiableBox : public MacroObject {
  public:
    DiableBox(array<double, 3> size_) : MacroObject() {
      size = size_;
    }

    void init(array<double, 3> pos_) {
      pos = pos_;
      G3Xcolor testcolor = {0.4, 0.7, 0.9};

      ClownFace* clowface = new ClownFace({pos[0], pos[1], pos[2]},
          {
          size[0], size[1], size[2]
          });
      clowface->init();

      ClownFace* clownnose = new ClownFace({pos[0] + size[0] / 1.5, pos[1] + size[1] / 1.5, pos[2]},
          {
          size[0] / 4, size[1] / 4, size[2] / 4
          });
      clownnose->init();
      clownnose->setColor(magenta);

      ClownFace* clownmouth = new ClownFace({pos[0] + size[0] / 1.8, pos[1] + size[1] / 1.8, pos[2] - size[2] / 2},
          {
          size[0] / 4, size[1] / 4, size[2] / 4 - size[2] / 7
          });
      clownmouth->init();
      clownmouth->setColor(testcolor);

      ClownFace* clowneyesl = new ClownFace({pos[0] + size[0], pos[1], pos[2] + size[2] / 2.5},
          {
          size[0] / 3 - size[0] / 7, size[1] / 3 - size[1] / 7, size[2] / 4 - size[2] / 7
          });
      clowneyesl->init();
      clowneyesl->setColor(green);

      ClownFace* clowneyesr = new ClownFace({pos[0], pos[1] + size[1], pos[2] + size[2] / 2.5},
          {
          size[0] / 3 - size[0] / 7, size[1] / 3 - size[1] / 7, size[2] / 4 - size[2] / 7
          });
      clowneyesr->init();
      clowneyesr->setColor(green);

      ClownCap* clowCap = new ClownCap({pos[0], pos[1], pos[2] + size[2] / 1.5},
          {
          size[0] - size[0] / 4, size[1] - size[1] / 4, size[2]
          });
      clowCap->init();
      clowCap->setColor(pink);

      ClownFace* clownEar = new ClownFace({pos[0] + size[0] / 1.5, pos[1] - size[1] / 1.5, pos[2]},
          {
          size[0] /2, size[1] / 2 , size[2] / 3
          });
      clownEar->init();
      clownEar->setColor(blue);

      ClownFace* clownEar2 = new ClownFace({pos[0] - size[0] / 1.5, pos[1] +size[1] / 1.5, pos[2]},
          {
          size[0] /2, size[1] / 2 , size[2] / 3
          });
      clownEar2->init();
      clownEar2->setColor(blue);

      ClownNeck * clownneck = new ClownNeck({pos[0] , pos[1] , pos[2]-2.3*size[2]},
          {
          size[0] /2, size[1] / 2 , size[2] / 2
          });
      clownneck->init();
      clownneck->setColor(blue);

      ClownTrunk * clownTrunk = new ClownTrunk({pos[0] , pos[1] , pos[2]-2.8*size[2]},
          {
          size[0], size[1] , size[2]*2.8
          });
      clownTrunk->init();
      clownTrunk->setColor(cyan);

      objects.push_back(clowface);
      objects.push_back(clownnose);
      objects.push_back(clownmouth);
      objects.push_back(clowneyesl);
      objects.push_back(clowneyesr);
      objects.push_back(clowCap);
      objects.push_back(clownEar);
      objects.push_back(clownEar2);
      objects.push_back(clownneck);
      objects.push_back(clownTrunk);
    }
};
