#include "CylinderCannonic.h"
#include <iostream>

#define NCylinder 100
#define PCylinder 100

int nCylinder = 30, pCylinder = 30;
double hauteurCylinder = 2;

void CylinderCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(NCylinder*PCylinder, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(NCylinder*PCylinder, sizeof (G3Xvector));
  double theta = 2 * PI / PCylinder;
  double phi = PI / NCylinder;
  double cosiphi, siniphi;
  int i, j;
  /**
   * initialisation des matrices de transformations
   */

  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 0;
  body.ambi = 0, 2;
  body.diff = 0.6;
  body.spec = 0.5;
  body.shine = 0.5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {1.0, 0., 1.0, 0.0};

  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];
  /**
   * on etablit les caracteristiques physiques de l'objet
   */
  double hauteur = 2, r;
  for (i = 0; i < NCylinder; i++) {
    r = i / (NCylinder / hauteur - 1.);
    for (j = 0; j < PCylinder; j++) {

      body.vrtx[i * PCylinder + j][0] = 1.0 * cos(j * theta);
      body.vrtx[i * PCylinder + j][1] = 1.0 * sin(j * theta);
      body.vrtx[i * PCylinder + j][2] = r - 1;
    }
  }

  body.scalex = 10;
  body.scaley = 10;
  memcpy(body.norm, body.vrtx, NCylinder * PCylinder * sizeof (G3Xvector));

  //g3x_CreateScrollv_i("n", &nCylinder, 3, NCylinder, 1.0, " ");
  //g3x_CreateScrollv_i("p", &pCylinder, 3, PCylinder, 1.0, " ");
  //g3x_CreateScrollh_d("r", &hauteurCylinder, 0.1, 10, 1.0, " ");

}

void CylinderCannonic::Draw() {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);
  glBegin(GL_QUADS);
  int k;
  int stepn = NCylinder / nCylinder;
  int stepp = PCylinder / pCylinder;
  int i, j;
  for (i = 0; i < NCylinder - stepn; i += stepn) {
    for (j = 0; j < PCylinder - stepp; j += stepp) {
      k = i * PCylinder + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = i * PCylinder + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PCylinder + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PCylinder + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PCylinder + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = i*PCylinder;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PCylinder;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PCylinder + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }

  glEnd();

  /*Pour boucler le trou d'en haut*/

  //Combler le trou den bas

  glBegin(GL_POLYGON);
  for (j = 0; j < PCylinder - stepp; j += stepp) {
    k = (NCylinder - stepn) * PCylinder + j % PCylinder;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }
  glNormal3dv(body.norm[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  glVertex3dv(body.vrtx[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCylinder - stepp; j += stepp) {
    k = PCylinder + j % PCylinder;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }
  glNormal3dv(body.norm[PCylinder + j % PCylinder]);
  glVertex3dv(body.vrtx[PCylinder + j % PCylinder]);
  glEnd();
}

void CylinderCannonic::Draw(HMat vrtx, HMat norm) {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);
  glBegin(GL_QUADS);

  G3Xpoint normal;
  G3Xvector vertex;

  int k;
  int stepn = NCylinder / nCylinder;
  int stepp = PCylinder / pCylinder;
  int i, j;
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  for (i = 0; i < NCylinder - stepn; i += stepn) {
    for (j = 0; j < PCylinder - stepp; j += stepp) {
      k = i * PCylinder + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = i * PCylinder + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = (i + stepn) * PCylinder + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = (i + stepn) * PCylinder + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PCylinder + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = i*PCylinder;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PCylinder;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PCylinder + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }

  glEnd();

  /*Pour boucler le trou d'en haut*/

  //Combler le trou den bas

  glBegin(GL_POLYGON);
  for (j = 0; j < PCylinder - stepp; j += stepp) {
    k = (NCylinder - stepn) * PCylinder + j % PCylinder;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }

  ProdMatPoint(vertex, vrtx, body.vrtx[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  ProdMatVector(normal, norm, body.norm[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  glNormal3dv(normal);
  glVertex3dv(vertex);

  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCylinder - stepp; j += stepp) {
    k = PCylinder + j % PCylinder;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }
  ProdMatPoint(vertex, vrtx, body.vrtx[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  ProdMatVector(normal, norm, body.norm[(NCylinder - stepn) * PCylinder + j % PCylinder]);
  glNormal3dv(normal);
  glVertex3dv(vertex);

  glEnd();
}

void CylinderCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void CylinderCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
