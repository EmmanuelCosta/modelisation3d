#include "CoilCannonic.h"

#define NCoil 100
#define PCoil 100

int nCoil = 30, pCoil = 30;
double hauteurCoil = 1;

void CoilCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(3 * NCoil*PCoil, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(3 * NCoil*PCoil, sizeof (G3Xvector));
  double theta = 2 * PI / PCoil;
  double phi = 3 * PI / NCoil;
  double cosiphi, siniphi;
  int i, j;
  /**
   * initialisation des matrices de transformations
   */
  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 1;
  body.ambi = 0, 2;
  body.diff = 0.3;
  body.spec = 0.4;
  body.shine = 0.5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {0.0, 1.0, 1.0, 0.0};

  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];

  double radius = 0.5;
  double hauteur = 0;
  /**
   * on etablit les caracteristiques physiques de l'objet
   */
  for (i = 0; i < 3 * NCoil; i++) {
    cosiphi = cos(i * phi);
    siniphi = sin(i * phi);
    hauteur = (i * phi) / 5;
    for (j = 0; j < PCoil; j++) {

      body.vrtx[i * PCoil + j][0] = cosiphi * (1 + radius * cos(j * theta));
      // body.norm[i * PCoil + j][0]= body.vrtx[i * PCoil + j][0] - cosiphi;
      body.vrtx[i * PCoil + j][1] = siniphi * (1 + radius * cos(j * theta));
      //  body.norm[i * PCoil + j][1]= body.vrtx[i * PCoil + j][1] - (siniphi);
      body.vrtx[i * PCoil + j][2] = radius * sin(j * theta) + hauteur;
      //body.norm[i * PCoil + j][2]=body.vrtx[i * PCoil + j][2];
    }
  }
  body.scalex = 10;
  body.scaley = 10;
  memcpy(body.norm, body.vrtx, 3 * NCoil * PCoil * sizeof (G3Xvector));

}

void CoilCannonic::Draw() {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  int k;
  int stepn = NCoil / nCoil;
  int stepp = PCoil / pCoil;
  int i, j;
  for (i = 0; i < 3 * NCoil - stepn; i += stepn) {
    for (j = 0; j < PCoil - stepp; j += stepp) {
      k = i * PCoil + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = i * PCoil + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PCoil + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * PCoil + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PCoil + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = i*PCoil;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PCoil;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * PCoil + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }

  glEnd();
}

void CoilCannonic::Draw(HMat vrtx, HMat norm) {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  int k;
  int stepn = NCoil / nCoil;
  int stepp = PCoil / pCoil;
  int i, j;
  G3Xpoint normal;
  G3Xvector vertex;
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  for (i = 0; i < 3 * NCoil - stepn; i += stepn) {
    for (j = 0; j < PCoil - stepp; j += stepp) {
      k = i * PCoil + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = i * PCoil + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * PCoil + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * PCoil + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * PCoil + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = i*PCoil;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PCoil;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * PCoil + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }

  glEnd();
}

void CoilCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void CoilCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
