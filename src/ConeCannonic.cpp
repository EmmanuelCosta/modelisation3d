#include "ConeCannonic.h"

#define NCone 100
#define PCone 100

int nCone = 30, pCone = 30;
double hauteurCone = 1;

void ConeCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(NCone*PCone, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(NCone*PCone, sizeof (G3Xvector));

  double theta = 2 * PI / PCone;
  double hauteur;
  double phi = PI / NCone;
  int i, j;

  /**
   * initialisation des matrices de transformations
   */
  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 0.9;
  body.ambi = 0, 1;
  body.diff = 1;
  body.spec = 0.5;
  body.shine = 5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {0.8, 0.6, 0.0, 0.0};

  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];
  /**
   * on etablit les caracteristiques physiques de l'objet
   */
  for (i = 0; i < NCone; i++) {
    hauteur = i / (NCone - 1.);
    for (j = 0; j < PCone; j++) {

      body.vrtx [i * PCone + j][0] = (1 - hauteur) * cos(j * theta);
      body.vrtx [i * PCone + j][1] = (1 - hauteur) * sin(j * theta);
      body.vrtx [i * PCone + j][2] = hauteur;
    }
  }
  memcpy(body.norm, body.vrtx, NCone * PCone * sizeof (G3Xvector));

  //g3x_CreateScrollv_i("no", &nCone, 3, NCone, 1.0, " ");
  //g3x_CreateScrollv_i("po", &pCone, 3, PCone, 1.0, " ");
  //g3x_CreateScrollh_d("ro", &hauteurCone, 0.1, 10, 1.0, " ");
}

void ConeCannonic::Draw() {
  int stepn = NCone / nCone, stepp = PCone / pCone;
  int i, j, k;

  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  glPointSize(2);
  glBegin(GL_POINTS);

  /* Pour GL_QUADS */
  for (i = 0; i < NCone - stepn; i += stepn) {
    for (j = 0; j < PCone - stepp; j += stepp) {
      k = i * PCone + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

      k = i * PCone + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

      k = (i + stepn) * PCone + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

      k = (i + stepn) * PCone + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

    }
    /* boucle de fin: pour fermer le cône j+stepp = 0, on revient au début donc en 0 */
    k = i * PCone + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);

    k = i*PCone;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);

    k = (i + stepn) * PCone;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);

    k = (i + stepn) * PCone + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }
  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCone - stepp; j += stepp) {
    k = (NCone - stepn) * PCone + j % PCone;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }
  glNormal3dv(body.norm[(NCone - stepn) * PCone + j % PCone]);
  glVertex3dv(body.vrtx[(NCone - stepn) * PCone + j % PCone]);
  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCone - stepp; j += stepp) {
    k = PCone + j % PCone;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }
  glNormal3dv(body.norm[PCone + j % PCone]);
  glVertex3dv(body.vrtx[PCone + j % PCone]);
  glEnd();
}

void ConeCannonic::Draw(HMat vrtx, HMat norm) {
  int stepn = NCone / nCone, stepp = PCone / pCone;
  int i, j, k;

  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);

  glBegin(GL_QUADS);
  glPointSize(2);
  glBegin(GL_POINTS);
  G3Xpoint normal;
  G3Xvector vertex;
  /* Pour GL_QUADS */
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  for (i = 0; i < NCone - stepn; i += stepn) {
    for (j = 0; j < PCone - stepp; j += stepp) {
      k = i * PCone + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = i * PCone + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = (i + stepn) * PCone + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

      k = (i + stepn) * PCone + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

    }
    /* boucle de fin: pour fermer le cône j+stepp = 0, on revient au début donc en 0 */
    k = i * PCone + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);

    k = i*PCone;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);

    k = (i + stepn) * PCone;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);

    k = (i + stepn) * PCone + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }
  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCone - stepp; j += stepp) {
    k = (NCone - stepn) * PCone + j % PCone;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }
  ProdMatPoint(vertex, vrtx, body.vrtx[(NCone - stepn) * PCone + j % PCone]);
  ProdMatVector(normal, norm, body.norm[(NCone - stepn) * PCone + j % PCone]);
  glNormal3dv(normal);
  glVertex3dv(vertex);

  glEnd();

  glBegin(GL_POLYGON);
  for (j = 0; j < PCone - stepp; j += stepp) {
    k = PCone + j % PCone;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }
  ProdMatPoint(vertex, vrtx, body.vrtx[(NCone - stepn) * PCone + j % PCone]);
  ProdMatVector(normal, norm, body.norm[(NCone - stepn) * PCone + j % PCone]);
  glNormal3dv(normal);
  glVertex3dv(vertex);
  glEnd();
}

void ConeCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void ConeCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
