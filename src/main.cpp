#include <cstdlib>
#include <stdio.h>
#include <g3x.h>
#include "SphereCannonic.h"
#include "CubeCannonic.h"
#include "CylinderCannonic.h"
#include "ToreCannonic.h"
#include"ConeCannonic.h"
#include "CoilCannonic.h"
#include "SceneExamples.cpp"
#include <unistd.h>

using namespace std;

void Anim();
void Draw();
void Exit();
void Init();

static double root_move_x = 0.0;
static double root_move_y = 0.0;
static double root_move_z = 0.0;
static double root_rot_alpha = 0.0;
static double root_rot_beta = 0.0;
static double root_rot_gamma = 0.0;

int main(int argc, char** argv) {
  g3x_InitWindow(*argv, 1200, 720);

  g3x_SetInitFunction(Init);
  g3x_SetDrawFunction(Draw);
  g3x_SetAnimFunction(Anim);
  g3x_SetExitFunction(Exit);

  g3x_SetPerspective(40., 100., 1.);
  g3x_SetLightPosition(0., 0., 10.);
  g3x_SetLightDirection(0., 0., 0.);

  g3x_CreateScrollv_d("x", &root_move_x, -0.05, 0.05, 1, "");
  g3x_CreateScrollv_d("y", &root_move_y, -0.05, 0.05, 1, "");
  g3x_CreateScrollv_d("z", &root_move_z, -0.05, 0.05, 1, "");
  g3x_CreateScrollv_d("x", &root_rot_alpha, -0.2, 0.2, 1, "");
  g3x_CreateScrollv_d("y", &root_rot_beta, -0.2, 0.2, 1, "");
  g3x_CreateScrollv_d("z", &root_rot_gamma, -0.2, 0.2, 1, "");

  return g3x_MainStart();
}

Node* root = new Node({0, 0, 0});
void Init() {
  Node* table_node1 = new Node({1, 3, 0});
  table_node1->set_macro_object(new Table({0.5, 0.5, 1.2}));

  Node* table_node3 = new Node({-2, -2, 0});
  table_node3->set_macro_object(new Table({1, 0.5, 1.2}));

  Node* stool1 = new Node({1, 1, 0});
  stool1->set_macro_object(new Stool({0.5, 0.5, 0.7}));
  Node* table_node2 = new Node({1, 0, 0});
  table_node2->set_macro_object(new Table({0.5, 0.5, 1.2}));

  Node* table_stool_node = new Node({0, 0, 0});
  table_stool_node->add_child(stool1);
  table_stool_node->add_child(table_node2);

  Node* wind = new Node({-2, 2, 1});
  wind->set_macro_object(new Wind({0.15, 0.15, 1}));

  Node* bed = new Node({1, -2, 1});
  bed->set_macro_object(new Bed({1.3, 0.8, 0.05}));

  Node* lamp = new Node({1, -2, 1.4});
  lamp->set_macro_object(new Lamp({0.15, 0.15, 0.15}));

  Node* diableBox = new Node({-2, 0, 2.4});
  diableBox->set_macro_object(new DiableBox({0.5, 0.5, 0.7}));

  root->add_child(table_node1);
  root->add_child(table_stool_node);
  root->add_child(table_node3);
  root->add_child(wind);
  root->add_child(bed);
  root->add_child(lamp);
  root->add_child(diableBox);

  root->init();
}

void Draw() {
  root->draw();
}

void Anim() {
  root->move(root_move_x, root_move_y, root_move_z);
  root->rotate(root_rot_alpha, root_rot_beta, root_rot_gamma);
}

void Exit() {
  root->exit();
}
