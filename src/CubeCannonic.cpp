#include "CubeCannonic.h"

#define NCube 100
#define PCube 100

int nCube = 54, pCube = 103;
double hauteurCube = 1;

void CubeCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(NCube*PCube, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(NCube*PCube, sizeof (G3Xvector));
  double theta = 2 * PI / PCube;
  double phi = PI / NCube;
  double cosiphi, siniphi;
  int i, j;
  /**
   * initialisation des matrices de transformations
   */
  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 0.9;
  body.ambi = 0, 1;
  body.diff = 1;
  body.spec = 0.5;
  body.shine = 5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {0.8, 0.6, 0.0, 0.0};
  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];

  body.scalex = 10;
  body.scaley = 10;
  memcpy(body.norm, body.vrtx, NCube * PCube * sizeof (G3Xvector));
}

void CubeCannonic::Draw() {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);
  glBegin(GL_QUADS);

  //face 1 : devant
  glNormal3d(0, 1, 0);

  glVertex3d(-1, 1, 1);
  glVertex3d(1, 1, 1);
  glVertex3d(1, 1, -1);
  glVertex3d(-1, 1, -1);

  //face 2 : audessus
  glNormal3d(0, 0, 1);

  glVertex3d(-1, 1, 1);
  glVertex3d(1, 1, 1);
  glVertex3d(1, -1, 1);
  glVertex3d(-1, -1, 1);

  //face 3 : derriere
  glNormal3d(0, -1, 0);

  glVertex3d(-1, -1, 1);
  glVertex3d(1, -1, 1);
  glVertex3d(1, -1, -1);
  glVertex3d(-1, -1, -1);

  //face 4 : en bas
  glNormal3d(0, 0, -1);

  glVertex3d(-1, 1, -1);
  glVertex3d(1, 1, -1);
  glVertex3d(1, -1, -1);
  glVertex3d(-1, -1, -1);

  //face 5 : gauche
  glNormal3d(-1, 0, 0);

  glVertex3d(-1, 1, 1);
  glVertex3d(-1, -1, 1);
  glVertex3d(-1, -1, -1);
  glVertex3d(-1, 1, -1);

  //face 6 : droit
  glNormal3d(1, 0, 0);

  glVertex3d(1, 1, 1);
  glVertex3d(1, -1, 1);
  glVertex3d(1, -1, -1);
  glVertex3d(1, 1, -1);

  glEnd();
}

void CubeCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void CubeCannonic::Draw(HMat vrtx, HMat norm) {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);
  glBegin(GL_QUADS);
  G3Xpoint normal;
  G3Xvector vertex;
  G3Xpoint n;
  G3Xvector v;
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  //face 1 :devant
  n[0] = 0;
  n[1] = 1;
  n[2] = 0;

  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);
  v[0] = -1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  v[0] = -1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  //face 2 : audessus
  n[0] = 0;
  n[1] = 0;
  n[2] = 1;
  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);

  v[0] = -1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  v[0] = 1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = -1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  //    //face 3 : derriere
  n[0] = 0;

  n[1] = -1;
  n[2] = 0;
  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);

  v[0] = -1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = -1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  //face 4 : en bas
  n[0] = 0;
  n[1] = 0;
  n[2] = -1;
  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);

  v[0] = -1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  v[0] = -1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  //face 5 : gauche
  n[0] = -1;
  n[1] = 0;
  n[2] = 0;
  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);

  v[0] = -1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = -1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = -1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = -1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  //    //face 6 : droit

  n[0] = 1;
  n[1] = 0;
  n[2] = 0;
  ProdMatPoint(normal, norm, n);
  glNormal3dv(normal);

  v[0] = 1;
  v[1] = 1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = -1;
  v[2] = 1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);
  v[0] = 1;
  v[1] = -1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  v[0] = 1;
  v[1] = 1;
  v[2] = -1;
  ProdMatVector(vertex, vrtx, v);
  glVertex3dv(vertex);

  glEnd();
}

void CubeCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
