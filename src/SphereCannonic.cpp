#include "SphereCannonic.h"
#include "ConeCannonic.h"
#define N 1000
#define P 1000
#include <iostream>
using namespace std;
int n = 54, p = 103;
double hauteur = 1;

void SphereCannonic::Init() {
  body.vrtx = (G3Xpoint*) calloc(N*P, sizeof (G3Xpoint));
  body.norm = (G3Xvector*) calloc(N*P, sizeof (G3Xvector));
  double theta = 2 * PI / P;
  double phi = PI / N;
  double cosiphi, siniphi;
  int i, j;
  /**
   * initialisation des matrices de transformations
   */
  NullMat(body.Md);
  NullMat(body.Mi);
  NullMat(body.Mn);
  /**
   * on initialise les variables d'environnement
   */
  body.alpha = 0.9;
  body.ambi = 0, 1;
  body.diff = 1;
  body.spec = 0.5;
  body.shine = 5;
  /**
   * on etablit la couleur par defaut
   */
  float f[4] = {0.8, 0.6, 0.0, 0.0};

  body.col[0] = f[0];
  body.col[1] = f[1];
  body.col[2] = f[2];
  body.col[3] = f[3];
  /**
   * on etablit les caracteristiques physiques de l'objet
   */
  for (i = 0; i < N; i++) {
    cosiphi = cos(i * phi);
    siniphi = sin(i * phi);
    for (j = 0; j < P; j++) {
      body.vrtx[i * P + j][0] = cos(j * theta) * siniphi;
      body.vrtx[i * P + j][1] = sin(j * theta) * siniphi;
      body.vrtx[i * P + j][2] = cosiphi;
    }
  }
  body.scalex = 10;
  body.scaley = 10;
  memcpy(body.norm, body.vrtx, N * P * sizeof (G3Xvector));
}

void SphereCannonic::Draw() {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);

  glBegin(GL_QUADS);
  int k;
  int stepn = N / n;
  int stepp = P / p;
  int i, j;
  for (i = 0; i < N - stepn; i += stepn) {
    for (j = 0; j < P - stepp; j += stepp) {
      k = i * P + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = i * P + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * P + (j + stepp);
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);
      k = (i + stepn) * P + j;
      glNormal3dv(body.norm[k]);
      glVertex3dv(body.vrtx[k]);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * P + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = i*P;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * P;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
    k = (i + stepn) * P + j;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);
  }

  glEnd();

  /*Pour boucler le trou d'en bas*/

  glBegin(GL_POLYGON);
  for (j = 0; j < P; j += stepp) {
    k = (N - stepn) * P + j % P;
    glNormal3dv(body.norm[k]);
    glVertex3dv(body.vrtx[k]);

  }

  glEnd();
}

void SphereCannonic::Exit() {
  free(body.vrtx);
  free(body.norm);
}

void SphereCannonic::Draw(HMat vrtx, HMat norm) {
  g3x_Material(body.col, body.ambi, body.diff,
          body.spec, body.shine, body.alpha);
  glPointSize(2);

  glBegin(GL_QUADS);
  int k;
  int stepn = N / n;
  int stepp = P / p;
  int i, j;
  G3Xpoint normal;
  G3Xvector vertex;
  /**
   * on dessine,
   * on multiplie chaque vertex de l'objet par vrtx donné en paramètre
   * et on fait de meme pour chacune de ses normales qu'oon multiplie par norm
   */
  for (i = 0; i < N - stepn; i += stepn) {
    for (j = 0; j < P - stepp; j += stepp) {
      k = i * P + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = i * P + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * P + (j + stepp);
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);
      k = (i + stepn) * P + j;
      ProdMatPoint(vertex, vrtx, body.vrtx[k]);
      ProdMatVector(normal, norm, body.norm[k]);
      glNormal3dv(normal);
      glVertex3dv(vertex);

    }
    /*pour combler l'espace vide du milieu de la sphere*/
    k = i * P + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = i*P;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * P;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
    k = (i + stepn) * P + j;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);
  }

  glEnd();

  /*Pour boucler le trou d'en bas*/
  glBegin(GL_POLYGON);
  for (j = 0; j < P; j += stepp) {
    k = (N - stepn) * P + j % P;
    ProdMatPoint(vertex, vrtx, body.vrtx[k]);
    ProdMatVector(normal, norm, body.norm[k]);
    glNormal3dv(normal);
    glVertex3dv(vertex);

  }

  glEnd();
}

void SphereCannonic::setColor(G3Xcolor color) {
  body.col[0] = color[0];
  body.col[1] = color[1];
  body.col[2] = color[2];
  body.col[3] = color[3];
}
