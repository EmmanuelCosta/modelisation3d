#include <array>
#include <stdio.h>
#include <vector>

using namespace std;

class Object {
  public:
    CanonicalObject* canonical_o;

    Object(array<double, 3> pos_) {
      pos = pos_;
    }

    virtual void init() = 0;
    virtual void draw() = 0;

    void setColor(G3Xcolor color){
      canonical_o->setColor(color);
    }

    virtual void move(double x, double y, double z) {
      vec[0] = x;
      vec[1] = y;
      vec[2] = z;
      HMat tmp;
      Translation(tmp, vec, true);
      ProdMatMat2(canonical_o->body.Md, tmp);
    }

    virtual void rotate(double alpha, double beta, double gamma) {
      HMat tmp;
      Rotation(tmp, alpha, beta, gamma, false);
      ProdMatMat2(canonical_o->body.Md, tmp);
    }
    
    virtual void exit() {   
      canonical_o->Exit();
    }
    G3Xvector vec;
    HMat transform;

  protected:
    array<double, 3> pos;
};

class MacroObject {
  public:
    array<double, 3> pos;

    virtual void init(array<double, 3> pos_) = 0;

    void draw() {
      for (Object* object : objects) object->draw();
    }

    void move(double x, double y, double z) {
      for (Object* object : objects) object->move(x, y, z);
    }

    void rotate(double alpha, double beta, double gamma) {
      for (Object* object : objects) object->rotate(alpha, beta, gamma);
    }
    
    void exit() {
      for (Object* object : objects) object->exit();
    }
  protected:
    array<double, 3> size;
    vector<Object*> objects;
};

class Node {
  public:
    array<double, 3> pos;

    Node(array<double, 3> pos_) {
      pos = pos_;
    }

    void init() {
      for (Node* node : children) node->init();
      if (macro_object != NULL) macro_object->init(pos);
    }

    void draw() {
      for (Node* node : children) node->draw();
      if (macro_object != NULL) macro_object->draw();
    }

    void move(double x, double y, double z) {
      for (Node* node : children) node->move(x, y, z);
      if (macro_object != NULL) macro_object->move(x, y, z);
    }

    void rotate(double alpha, double beta, double gamma) {
      for (Node* node : children) node->rotate(alpha, beta, gamma);
      if (macro_object != NULL) macro_object->rotate(alpha, beta, gamma);
    }

    void add_child(Node* node) {
      node->pos[0] += pos[0];
      node->pos[1] += pos[1];
      node->pos[2] += pos[2];
      children.push_back(node);
    }

    void set_macro_object(MacroObject* mo) {
      macro_object = mo;
      macro_object->pos[0] += pos[0];
      macro_object->pos[1] += pos[1];
      macro_object->pos[2] += pos[2];
    }
    
    void exit() {
      for (Node* node : children) node->exit();
      if (macro_object != NULL) macro_object->exit();
    }

  protected:
    MacroObject* macro_object;
    vector<Node*> children;
};
