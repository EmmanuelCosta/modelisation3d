#include "Functions.h"

/**
 * initilalise la matrice donnée en parametre en 
 * matrice identite
 * @param identity
 */
void Identity(HMat identity) {
  for (int i = 0; i < 16; i++) identity[i] = 0;
  identity[0] = 1;
  identity[5] = 1;
  identity[10] = 1;
  identity[15] = 1;
}
/**
 * initilalise la matrice donnée en parametre en 
 * matrice nulle
 * @param identity
 */
void NullMat(HMat nullMat) {
  for (int i = 0; i < 16; i++) nullMat[i] = 0;
}

/**
 * effectue une translation suivant le vecteur v
 * result est la matrice de translation
 * si reset à true, la matrice sera d'abord transformée en matrice identité
 * @param result
 * @param v
 * @param reset
 */
void Translation(HMat result, G3Xvector v,bool reset) {
  if (reset) Identity(result);
  result[3] = v[0];
  result[7] = v[1];
  result[11] = v[2];
  result[15] = 1;
}
/**
 * B=M*A
 * multiplie la matrice M par le point A.
 * B en est le resultat
 * @param B
 * @param M
 * @param A
 */
void ProdMatPoint(G3Xpoint B, HMat M, G3Xpoint A) {
  int i, j = 0;
  for (i = 0; i < 16; i = i + 4) {
    B[j] = M[i] * A[0] + M[i + 1] * A[1] + M[i + 2] * A[2] + M[i + 3]*1;
    j++;
  }
}
/**
 * V=M*U
 * multiplie la matrice M par le vecteur U
 * V en est le resultat
 * @param V
 * @param M
 * @param U
 */
void ProdMatVector(G3Xvector V, HMat M, G3Xvector U) {
  int i, j = 0;
  for (i = 0; i < 16; i = i + 4) {
    V[j] = M[i] * U[0] + M[i + 1] * U[1] + M[i + 2] * U[2] + M[i + 3]*1;
    j++;
  }
}

/**
 * effectue la rotation suivant les axes x, y et z avec les axes alpha, beta et gamma
 * @param result
 * @param alpha
 * @param beta
 * @param gamma
 * @param reset
 */
void Rotation(HMat result, double alpha, double beta, double gamma, bool reset) {
  result[0] = cos(alpha) * cos(beta);
  result[1] = cos(alpha) * sin(beta) * sin(gamma) - sin(alpha) * cos(gamma);
  result[2] = cos(alpha) * sin(beta) * cos(gamma) + sin(alpha) * sin(gamma);
  result[3] = 0;
  result[4] = sin(alpha) * cos(beta);
  result[5] = sin(alpha) * sin(beta) * sin(gamma) + cos(alpha) * cos(gamma);
  result[6] = sin(alpha) * sin(beta) * cos(gamma) - cos(alpha) * sin(gamma);
  result[7] = 0;
  result[8] = -sin(beta);
  result[9] = cos(beta) * sin(gamma);
  result[10] = cos(beta) * cos(gamma);
  result[11] = 0;
  result[12] = 0;
  result[13] = 0;
  result[14] = 0;
  result[15] = 1;
}
/**
 * rotation sur l'axe des x
 * @param result
 * @param alpha
 * @param reset
 */
void RotationX(HMat result, double alpha,bool reset) {
  if (reset) Identity(result);
  result[5] = cos(alpha);
  result[6] = -sin(alpha);
  result[9] = sin(alpha);
  result[10] = cos(alpha);
}
/**
 * rotation sur l'axe des y
 * @param result
 * @param alpha
 * @param reset
 */
void RotationY(HMat result, double alpha,bool reset) {
  if (reset) {
    Identity(result);
  }
  result[0] = cos(alpha);
  result[2] = sin(alpha);
  result[8] = -sin(alpha);
  result[10] = cos(alpha);
}
/**
 * rotation sur l'axe des z
 * @param result
 * @param alpha
 * @param reset
 */
void RotationZ(HMat result, double alpha,bool reset) {
  if (reset) {
    Identity(result);
  }
  result[0] = cos(alpha);
  result[1] = -sin(alpha);
  result[4] = sin(alpha);
  result[5] = cos(alpha);
}
/**
 * rotation sur l'axe des Z
 * @param result
 * @param alpha
 * @param reset
 */
void RotationZ_custom(HMat result, double alpha,bool reset) {
  if (reset) {
    Identity(result);
  }
  result[0] = -cos(alpha);
  result[1] = sin(alpha);
  result[4] = -sin(alpha);
  result[5] = cos(alpha);
}

// TODO: Remove the reset arg and reset all the time
void Scaling(HMat result, double hx, double hy, double hz, bool reset) {
  if (reset) {
    Identity(result);
  }

  result[0] = hx;
  result[5] = hy;
  result[10] = hz;
  result[15] = 1;
}

/**
 * produit lmatriciel
 * @param result = A*B
 * @param A
 * @param B
 */
void ProdMatMat(HMat result, HMat A, HMat B) {
  int i, j, k;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      result[i * 4 + j] = 0;
      for (k = 0; k < 4; k++) {
        result[i*4+j] += B[i*4+k] * A[k*4+j];
      }
    }
  }
}

void ProdMatMat2(HMat A, HMat B){
  HMat result;
  NullMat(result);
  ProdMatMat( result, A,  B);

  for(int i = 0;i<16;i++) A[i]=result[i];
}

void printMatri(HMat A){
  for(int i = 0; i < 16; i++) {
    if (i % 4 == 0) puts("\n");
    printf("%f\t", A[i]);
  }
  puts("\n");
}
