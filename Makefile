CC = g++

ifeq ($(cc),g++)
  EXT = ++$(PROC)
else
  EXT = $(PROC)
endif

ifeq ($(DEBUG),yes)
  CFLAGS  = -g -Wpointer-arith -Wall -ansi $(INC_MTRACER)=0 -std=c++0x -Wno-write-strings
  DBG_LIB = $(LIB_MTRACER)
  DBG = .dbg
else
  CFLAGS  = -O2 -ansi -std=c++0x -Wno-write-strings
  DBG_LIB = 
  DBG = 
endif

lib = -L/usr/lib $(libG3X) 
inc = -I./include  $(incG3X) 
src = src/
bin = bin/

all: projet clean

projet: Functions.o SphereCannonic.o CylinderCannonic.o ToreCannonic.o CoilCannonic.o ConeCannonic.o CubeCannonic.o main.o
		@$(CC) $(CFLAGS)  $(lib) -o $(bin)projet Functions.o SphereCannonic.o CoilCannonic.o CylinderCannonic.o ToreCannonic.o ConeCannonic.o CubeCannonic.o main.o

# règle générique de création de xxx.o à partir de src/xxx.c
%.o : $(src)%.cpp
	@echo "module $@"
	@$(CC) $(CFLAGS) $(inc) -c $< -o $@
	@echo "------------------------"

# règle générique de création de l'executable xxx à partir de src/xxx.c (1 seul module)
main: main.o
	@echo "module "
	@$(CC) $(CFLAGS) $(inc) -c $(src)main.cpp -o main.o
	@echo "------------------------"

	
.PHONY : clean cleanall ?

# informations sur les paramètres de compilation       
? :
	@echo "---------infos de compilation----------"
	@echo "  processeur     : $(PROCBIT)"
	@echo "  compilateur    : $(CC)"
	@echo "  options        : $(CFLAGS)"
	@echo "  lib g3x/OpenGl : $(libG3X)$(COMP)"
	@echo "  headers        : $(incG3X)"
ifeq ($(DBG),yes)
	@echo "  traceur ACTIF  : $(DBG_LIB)"
	@echo "                   $(DBG_FLAG)"
endif

clean : 
	@rm -f *.o
cleanall :
	@rm -f *.o bin/projet
	
