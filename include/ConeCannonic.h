#ifndef CONECANNONIC_H
#define CONECANNONIC_H

#include "CanonicalObject.h"
#include "SceneElement.h"

class ConeCannonic : public CanonicalObject {
  public:
    void Init();
    void Draw();
    void Draw(HMat vrtx, HMat norm);
    void Exit();
    void setColor(G3Xcolor color);
};

#endif
