#ifndef CUBECANNONIC_H
#define CUBECANNONIC_H

#include "CanonicalObject.h"
#include "SceneElement.h"

class CubeCannonic : public CanonicalObject {
  public:
    void Init();
    void Draw();
    void Draw(HMat vrtx, HMat norm);
    void Exit();
    void setColor(G3Xcolor color);
};

#endif
