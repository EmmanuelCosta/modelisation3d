#ifndef CylinderCannonic_H
#define CylinderCannonic_H

#include "CanonicalObject.h"
#include "SceneElement.h"

class CylinderCannonic: public CanonicalObject {
  public:
    void Init();
    void Draw();
    void Exit();
    void Draw(HMat vrtx, HMat norm);
    void setColor(G3Xcolor color);
};

#endif
