#include <g3x.h>

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

/**
 * represente la matrice
 */
typedef double HMat[16];

/**
 * represente un objet de la scene
 */
typedef struct {
  G3Xpoint *vrtx;   /*pointeur vers tableau de vertex*/
  G3Xvector *norm;  /*pointeur vers tableau de normales*/
  HMat Md, Mi, Mn;  /*matrices 4*4 directe/inverse/normarle.*/
  G3Xcolor col;
  double ambi, diff, spec, shine, alpha;  /*matériau*/
  double scalex, scaley;    /*facteurs d'échelle*/
} SceneElement;

#endif
