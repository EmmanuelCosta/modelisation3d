#ifndef SPHERECANNONIC_H
#define SPHERECANNONIC_H

#include "CanonicalObject.h"
#include "SceneElement.h"

class SphereCannonic: public CanonicalObject {
  public:
    void Draw();
    void Init();
    void Exit();
    void Draw(HMat vrtx, HMat norm);
    void setColor(G3Xcolor color);
};

#endif
