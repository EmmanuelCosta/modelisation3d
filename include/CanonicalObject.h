#ifndef CANONICALOBJECT_H
#define CANONICALOBJECT_H

#include "SceneElement.h"
#include "Functions.h"

static G3Xcolor red = {1.0, 0.0, 0.0};
static G3Xcolor yellow = {1.0, 1.0, 0.0};
static G3Xcolor green = {0.0, 1.0, 0.0};
static G3Xcolor cyan = {0.0, 1.0, 1.0};
static G3Xcolor blue = {0.0, 0.0, 1.0};
static G3Xcolor magenta = {1.0, 0.0, 1.0};
static G3Xcolor pink = {1, 0, 0.3};

/**
 * cette Interface definit le contrat que doit 
 * respecter une objet cannonique
 */
class CanonicalObject {
  public:
    /**
     * 
     */
    SceneElement body;
/**
 * c'est la fonction qui initialise l'objet on y determinant ces caracteristiques
 */
    virtual void Init() = 0;
    /**
     * C'est la fonction dans laquelle sera dessiner les objtes
     */
    virtual void Draw() = 0;
    /**
     * cette fonction dessine l'objet en tenant compte de la matrice vrtx pour transformer les points
     * et la matrice norm pour la lumiere
     * @param vrtx : matrice de transformation des vertex de l'objet
     * @param norm : matrice de transformation de la lumière
     */
    virtual void Draw(HMat vrtx, HMat norm) = 0;
    
    /**
     * dans cette matrice devront être libérée les ressources allouées dynamiquement
     */
    virtual void Exit() = 0;
    /**
     * 
     * @param color
     */
    virtual void setColor(G3Xcolor color) = 0;
};

#endif
