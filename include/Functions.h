#ifndef IDENTITY_H
#define IDENTITY_H

#include "SceneElement.h"
void Identity(HMat identity);
void NullMat(HMat nullMat);
void Translation(HMat result, G3Xvector V, bool reset);
void Rotation(HMat result, double alpha, double beta, double gamma, bool reset);
void RotationX(HMat result,double alpha, bool reset);
void RotationY(HMat result,double alpha, bool reset);
void RotationZ(HMat result,double alpha, bool reset);
void RotationZ_custom(HMat result, double alpha,bool reset);
void Scaling(HMat result,double hx, double hy,double hz, bool reset);

/**
 * B = M * A
 */
void ProdMatPoint(G3Xpoint B, HMat M, G3Xpoint A);

/**
 * V = M * U
 */
void ProdMatVector(G3Xvector V, HMat M, G3Xvector U);

/**
 * result = A * B
 */
void ProdMatMat(HMat result, HMat A, HMat B);

/**
 * A = A * B
 */
void ProdMatMat2(HMat A, HMat B);

void printMatrix(HMat A);
#endif
